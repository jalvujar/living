import { shallowMount } from "@vue/test-utils";
import CitySelector from "@/components/CitySelector.vue";

test("Verificar lista de ciudades", () => {
  const cityListForTesting = [
    {
      name: "Bilbao",
      lat: "43.2627106",
      long: "-2.9252801"
    },
    {
      name: "Donosti",
      lat: "43.2918100",
      long: " -1.9885100"
    },
    {
      name: "Gasteiz",
      lat: "42.8499794",
      long: "-2.6726799"
    },
    {
      name: "Paris",
      lat: "42.8499794",
      long: "-2.6726799"
    }
  ];

  const wrapper = shallowMount(CitySelector, {
    propsData: {
      cityList: cityListForTesting
    }
  });
  const cityNodes = wrapper.findAll(".city").wrappers;

  expect(cityNodes[0].text()).toBe("Bilbao");
  expect(cityNodes[1].text()).toBe("Donosti");
  expect(cityNodes[2].text()).toBe("Gasteiz");
  expect(cityNodes[3].text()).toBe("Paris");
});

test("emite evento click", async () => {
  const cityList = [
    {
      name: "Bilbao",
      lat: "43.2627106",
      long: "-2.9252801"
    },
    {
      name: "Donosti",
      lat: "43.2918100",
      long: " -1.9885100"
    },
    {
      name: "Gasteiz",
      lat: "42.8499794",
      long: "-2.6726799"
    },
    {
      name: "Paris",
      lat: "42.8499794",
      long: "-2.6726799"
    }
  ];
  const wrapper = shallowMount(CitySelector, {
    propsData: { cityList: cityList }
  });

  expect(wrapper.emitted().cityClick).toBe(undefined);

  const div = wrapper.find(".city");
  div.trigger("click");
  await wrapper.vm.$nextTick();
  expect(wrapper.emitted().cityClick.length).toBe(1);
  expect(wrapper.emitted().cityClick[0]).toEqual([
    {
      name: "Bilbao",
      lat: "43.2627106",
      long: "-2.9252801"
    }
  ]);
});
