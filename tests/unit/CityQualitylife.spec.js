import { shallowMount } from "@vue/test-utils";
import CityQualitylife from "@/components/CityQualitylife.vue";

test("Verificar lista de calidad e vida", () => {
  const CityQualitylisttest = [
    {},
    {
      name: "Bilbao",
      Description:
        "Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño",
      Imagen: "Img_2.jpg"
    },
    {
      name: "Donosti",
      Description:
        "Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño",
      Imagen: "Img_2.jpg"
    },
    {
      name: "Gasteiz",
      Description:
        "Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño",
      Imagen: "Img_3.jpg"
    },
    {
      name: "Paris",
      Description:
        "Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño",
      Imagen: "Img_4.jpg"
    }
  ];

  const wrapper = shallowMount(CityQualitylife, {
    propsData: {
      CityQuality: CityQualitylisttest
    }
  });
  const cityqualityNodes = wrapper.findAll(".quality").wrappers;

  expect(cityqualityNodes[0].text()).toBe("Bilbao");
  expect(cityqualityNodes[1].text()).toBe("Donosti");
  expect(cityqualityNodes[2].text()).toBe("Gasteiz");
  expect(cityqualityNodes[3].text()).toBe("Paris");
});

test("emite evento click", async () => {
  const CityQualitylisttest = [
    {
      name: "Bilbao",
      Description:
        "Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño",
      Imagen: "Img_1.jpg"
    },
    {
      name: "Donosti",
      Description:
        "Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño",
      Imagen: "Img_2.jpg"
    },
    {
      name: "Gasteiz",
      Description:
        "Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño",
      Imagen: "Img_3.jpg"
    },
    {
      name: "Paris",
      Description:
        "Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño",
      Imagen: "Img_4.jpg"
    }
  ];

  const wrapper = shallowMount(CityQualitylife, {
    propsData: {
      CityQuality: CityQualitylisttest
    }
  });

  expect(wrapper.emitted().onClickquality).toBe(undefined);
  const div = wrapper.findAll(".quality").wrappers;

  div[0].trigger("click");
  await wrapper.vm.$nextTick();

  expect(wrapper.emitted().onClickquality.length).toBe(1);
  expect(wrapper.emitted().onClickquality[0]).toEqual([
    {
      name: "Bilbao",
      Description:
        "Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño",
      Imagen: "Img_1.jpg"
    }
  ]);
});
